package common

import (
	"github.com/golang-jwt/jwt/v4"
)

// token中携带的消息体
type MyCustomClaims struct {
	UserId   int64  `json:"user_id"`
	Nickname string `json:"nickname"`
	jwt.RegisteredClaims
}
