package common

import "helper/common/errno"

/**
backend code 110000+
*/
const (
	// 身份认证域 code 110000+
	ErrCreateToken     = 110001 // 初始化token失败
	ErrRequestTokenNil = 110002 // 用户未登录
	ErrIllegalToken    = 110003 // token异常，身份认证失败
	ErrLoginTimeout    = 110004 // 登录超时
	ErrUserInfo        = 110005 // 用户信息异常
)

func init() {
	errno.Register(500, ErrCreateToken, "初始化token失败")
	errno.Register(401, ErrRequestTokenNil, "用户未登录")
	errno.Register(401, ErrIllegalToken, "token异常，身份认证失败")
	errno.Register(401, ErrLoginTimeout, "登录超时")
	errno.Register(401, ErrUserInfo, "用户信息异常")
}
