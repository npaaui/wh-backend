package common

import (
	"github.com/gin-gonic/gin"
	"helper/common/errno"
)

type RespBody struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func WriteResponse(c *gin.Context, err error, data interface{}) {
	if data == nil { // 避免接口返回null
		data = struct{}{}
	}
	codeErr := errno.NewCodeErr(errno.ErrNetwork)
	if err == nil {
		codeErr = errno.NewCodeErr(errno.Success)

	} else if tmp, ok := err.(errno.CodeErr); ok {
		codeErr = tmp
	}
	c.JSON(codeErr.GetHttpStatus(), RespBody{
		Code: codeErr.GetCode(),
		Msg:  codeErr.GetMsg(),
		Data: data,
	})
	return
}
