package common

import (
	"github.com/gin-gonic/gin"
	"github.com/gookit/validate"

	"helper/common/errno"
	"helper/common/logger"
)

/********************
 * 参数处理
 ********************/
func ValidByStruct(c *gin.Context, s interface{}) {
	err := c.Bind(s)
	if err != nil {
		logger.WithReqIdAndCode(c.GetInt64("req_id"), errno.ErrValid).Panicf(err.Error())
	}
	err = c.BindUri(s)
	if err != nil {
		logger.WithReqIdAndCode(c.GetInt64("req_id"), errno.ErrValid).Panicf(err.Error())
	}
	va := validate.Struct(s)
	if !va.Validate() {
		logger.WithReqIdAndCode(c.GetInt64("req_id"), errno.ErrValid).WithField("show", true).Panicf(va.Errors.One())
	}
}

func PartPage(page, pageSize int) (offset, limit int) {
	if page < 0 {
		page = 1
	}
	if pageSize > 200 || pageSize < 1 {
		pageSize = 10
	}
	limit = pageSize
	offset = limit * (page - 1)
	return offset, limit
}
