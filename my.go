package main

import (
	"fmt"
	_ "net/http/pprof"
)

type person struct {
	name string
}

func (p person) sayName() {
	p.name = "zzz"
	fmt.Println(p.name)
}

func (p *person) sayName2() {
	p.name = "zzz"
	fmt.Println(p.name)
}

func main() {
	zxc := &person{
		name: "zxc",
	}
	zxc.sayName()
	fmt.Println(zxc.name)
}
