# wh_backend
> 计算器项目后端 golang

## 目录
- [包依赖](#包依赖)
- [报错与日志](#报错与日志)
- [登录身份认证 JWT](#jwt)
- [接口文档 swagger](#swagger)

## 包依赖
项目使用 go module 管理依赖包，较于 go path 方式可以实现版本控制，规范项目包依赖

初始化项目时先执行 `go mod tidy` 更新包依赖

## 报错与日志
##### 统一code使用
错误code统一通过helper/common/errno定义，各服务需在common/code文件注册错误码 errno.Register()

code为六位int类型，**10** [不同服务] + **00** [服务中不同域] + **00** [不同错误]

##### 日志说明
[传送门 -> github.com/sirupsen/logrus](https://github.com/sirupsen/logrus)

日志统一为六个级别：Trace, Debug, Info, Warning, Error, Fatal, Panic；每个级别会分别记录在单个日志文件。

```go
log.Trace("Something very low level.")
log.Debug("Useful debugging information.")
log.Info("Something noteworthy happened!")
log.Warn("You should probably take a look at this.")
log.Error("Something failed but I'm not quitting.")
// Calls os.Exit(1) after logging
log.Fatal("Bye.")
// Calls panic() after logging
log.Panic("I'm bailing.")
```

可在`config.ini/logger.level`配置最低报错级别，本地开发可使用debug级别，上线可使用info级别。

为每个请求都初始化了一个`req_id`,写日志时最好携带上。例如：

```go
logger.WithReqId(c.GetInt64("req_id")).Debugf("%v %v", claims.UserId, claims.RegisteredClaims.ExpiresAt)
```

记录panic级别的日志时请求也会panic，middleware/recover中会抓取，最好携带code方便接口返回错误。例如：

```go
logger.WithReqIdAndCode(c.GetInt64("req_id"), common.ErrCreateToken).Panicf("token signed err: %v", err)
```

## JWT
[传送门 -> github.com/golang-jwt/jwt](https://github.com/golang-jwt/jwt)
##### 使用说明
`config.ini/auth.token_expire_time`配置token过期时间，每次请求均会重置过期时间
    
## Swagger
[传送门 -> github.com/swaggo/swag](https://github.com/swaggo/swag#readme) 

[传送门 -> swagger格式标准](https://editor.swagger.io/)
```shell
# 安装：
$ go get -u github.com/swaggo/swag/cmd/swag

# 检测是否安装成功：
$ swag -v
swag version v1.8.6

# 常用命令
# 格式化swag注解
$ swag fmt
# 自动生成接口文档 文档位于./docs/
$ swag init

# 文档访问路由：
{{host}}/dosc/index.html
```
##### 使用说明
接口的swagger注解需要紧跟在接口代码之上

注解写好后执行 `swag fmt && swag init` 自动更新文档，需要重启服务生效