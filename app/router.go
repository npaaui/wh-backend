package app

import (
	"github.com/gin-gonic/gin"

	v1 "wh_backend/app/controller/v1"
	"wh_backend/middleware"
)

func AddRouter(r *gin.Engine) {
	userCtr := v1.NewUserCtr()
	smsCtr := v1.NewSmsCtr()
	otherCtr := v1.NewOtherCtr()
	apiV1 := r.Group("api/v1/").Use(middleware.Logger()).Use(middleware.RecoverError())
	{
		apiV1.GET("other/about_we", otherCtr.AboutWe)        // 关于我们
		apiV1.POST("sms/sms_valid", smsCtr.SendSmsValidCode) // 发送短信验证码
		apiV1.GET("user/login", userCtr.Login)               // 登录

		apiV1.Use(middleware.Auth())
		{
			apiV1.GET("user", userCtr.InfoUser) // 用户详情
		}
	}
}
