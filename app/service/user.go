package service

import (
	"sync"

	"github.com/spf13/viper"

	"helper/service"
)

var UserServiceClient *service.ServiceClient
var UserServiceOnce sync.Once

func UserService() *service.ServiceClient {
	UserServiceOnce.Do(func() {
		UserServiceClient = service.NewServiceClient(viper.GetString("service.user_service"))
	})
	return UserServiceClient
}

// 用户详情
type InfoUserArgs struct {
	Id       int64  `json:"id"`
	Mobile   string `json:"mobile"`
	Register bool   `json:"register"`
}
