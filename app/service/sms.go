package service

import (
	"sync"

	"github.com/spf13/viper"

	"helper/service"
)

var SmsServiceClient *service.ServiceClient
var SmsServiceOnce sync.Once

func SmsService() *service.ServiceClient {
	SmsServiceOnce.Do(func() {
		SmsServiceClient = service.NewServiceClient(viper.GetString("service.sms_service"))
	})
	return SmsServiceClient
}

// 发送短信验证码
type SendSmsValidCodeArgs struct {
	Mobile string `json:"mobile" form:"mobile" uri:"mobile" validate:"required" label:"手机号"`
}

// 校验验证码
type ValidCodeArgs struct {
	Mobile string `json:"mobile" form:"mobile" uri:"mobile" validate:"required" label:"手机号"`
	Code   string `json:"code" form:"code" uri:"code" validate:"required" label:"验证码"`
}

// 获取验证码列表
type ListSmsValidArgs struct {
	Mobile string `json:"mobile" form:"mobile" uri:"mobile" validate:"required" label:"手机号"`
	Limit  int    `json:"limit" form:"limit" uri:"limit" validate:"required"`
	Offset int    `json:"offset" form:"offset" uri:"offset" validate:"required"`
}
