package v1

import (
	"github.com/gin-gonic/gin"

	"wh_backend/app/service"
	"wh_backend/common"
)

type SmsCtr struct{}

func NewSmsCtr() *SmsCtr {
	return &SmsCtr{}
}

/********************
 * 发送短信验证码
 ********************/

// @Summary     发送短信验证码
// @description 用于登录时发送短信验证码
// @Tags        sms
// @Accept      json
// @Produce     json
// @param       mobile          body     service.SendSmsValidCodeArgs true "手机号"
// @Success     200             {object} common.RespBody
// @Router      /sms/sms_valid  [post]
func (ctr *SmsCtr) SendSmsValidCode(c *gin.Context) {
	args := &service.SendSmsValidCodeArgs{}
	common.ValidByStruct(c, args)

	data, codeErr := service.SmsService().Call("SendSmsValidCode", args)
	if codeErr.NotNil() {
		common.WriteResponse(c, codeErr, nil)
		return
	}

	common.WriteResponse(c, nil, data)
	return
}
