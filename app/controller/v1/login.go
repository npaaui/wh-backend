package v1

import (
	"encoding/json"
	"github.com/golang-jwt/jwt/v4"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"helper/common/logger"

	"wh_backend/app/service"
	"wh_backend/common"
)

/********************
 * 登录
 ********************/
type LoginArgs struct {
	Mobile string `json:"mobile" form:"mobile" uri:"mobile" validate:"required|len:11" message:"len:请正确输入11位手机号" label:"手机号"`
	Code   string `json:"code" form:"code" uri:"code" validate:"required|len:6" message:"len:请正确输入6位验证码" label:"验证码"`
}

// @Summary 登录
// @Tags    user
// @Accept  json
// @Produce json
// @param   mobile       query    string true "手机号"
// @param   code         query    string true "短信验证码"
// @Success 200          {object} common.RespBody
// @Failure 500          {object} common.RespBody "110001:获取token失败; 130001:短信验证码发送失败; 130002:短信验证码超时; 130003:短信验证码错误"
// @Router  /user/login  [get]
func (ctr *UserCtr) Login(c *gin.Context) {
	args := &LoginArgs{}
	common.ValidByStruct(c, args)

	// 手机号校验验证码
	_, codeErr := service.SmsService().Call("ValidCode", service.ValidCodeArgs{Mobile: args.Mobile, Code: args.Code})
	if codeErr.NotNil() {
		common.WriteResponse(c, codeErr, nil)
		return
	}

	// 获取用户信息
	user, codeErr := service.UserService().CallMapItf("InfoUser", service.InfoUserArgs{Mobile: args.Mobile, Register: true})
	if codeErr.NotNil() {
		common.WriteResponse(c, codeErr, nil)
		return
	}

	// 生成token
	userId := int64(0)
	if tmpUserId, err := user["id"].(json.Number).Int64(); err != nil || tmpUserId == 0 {
		logger.WithReqIdAndCode(c.GetInt64("req_id"), common.ErrUserInfo).Panicf("user info err: %v", err)
	} else {
		userId = tmpUserId
	}
	tokenString := _getToken(c, userId, user["nickname"].(string))

	common.WriteResponse(c, nil, map[string]interface{}{
		"token": tokenString,
	})
	return
}

// 生成token
func _getToken(c *gin.Context, userId int64, nickname string) string {
	// 过期时间
	expireAt := time.Now().Add(time.Second * time.Duration(viper.GetInt("auth.token_expire_time")*86400))
	// 加密字串
	secret := viper.GetString("auth.token_secret")
	// 获取token
	claims := common.MyCustomClaims{
		UserId:   userId,
		Nickname: nickname,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expireAt),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &claims)
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		logger.WithReqIdAndCode(c.GetInt64("req_id"), common.ErrCreateToken).Panicf("token signed err: %v", err)
	}
	return tokenString
}
