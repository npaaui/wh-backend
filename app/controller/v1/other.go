package v1

import (
	"wh_backend/common"
	"github.com/gin-gonic/gin"
)

type OtherCtr struct{}

func NewOtherCtr() *OtherCtr {
	return &OtherCtr{}
}

func (ctr *OtherCtr) AboutWe(c *gin.Context) {
	common.WriteResponse(c, nil, map[string]interface{}{
		"version": "1.0",
	})
}
