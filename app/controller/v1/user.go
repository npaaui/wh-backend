package v1

import (
	"wh_backend/app/service"
	"wh_backend/common"
	"github.com/gin-gonic/gin"
)

type UserCtr struct{}

func NewUserCtr() *UserCtr {
	return &UserCtr{}
}

/********************
 * 获取用户详情
 ********************/
type InfoUserRet struct {
	Id         string `json:"id"`          // 用户编号
	HeadImg    string `json:"head_img"`    // 头像
	Mobile     string `json:"mobile"`      // 手机号
	Nickname   string `json:"nickname"`    // 昵称
	CreateTime string `json:"create_time"` // 创建时间
	UpdateTime string `json:"update_time"` // 更新时间
}

// @Summary 获取用户详情
// @Tags    user
// @Accept  json
// @Produce json
// @param   id           path string true "用户id"
// @Success 200 {object} common.RespBody{data=v1.InfoUserRet}
// @Router  /user/:id [get]
func (ctr *UserCtr) InfoUser(c *gin.Context) {
	args := &service.InfoUserArgs{
		Id: c.GetInt64("user_id"),
	}

	data, codeErr := service.UserService().Call("InfoUser", args)
	if codeErr.NotNil() {
		common.WriteResponse(c, codeErr, nil)
		return
	}

	common.WriteResponse(c, nil, data)
	return
}
