package middleware

import (
	"errors"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/spf13/viper"

	"wh_backend/common"
	"helper/common/errno"
	"helper/common/logger"
)

func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 获取header中的token
		tokenString := c.Request.Header.Get("authorization")
		tokenString = strings.Replace(tokenString, "Bearer ", "", 1)
		if tokenString == "" {
			panic(errno.NewCodeErr(common.ErrRequestTokenNil))
			c.Abort()
			return
		}

		// 解析token
		token, err := jwt.ParseWithClaims(tokenString, &common.MyCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte(viper.GetString("auth.token_secret")), nil
		})

		// 判断token有效性
		if token.Valid {

		} else if errors.Is(err, jwt.ErrTokenMalformed) {
			logger.WithReqIdAndCode(c.GetInt64("req_id"), common.ErrIllegalToken).Panicf("parse token err: %v", err)
			c.Abort()
			return
		} else if errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet) {
			logger.WithReqIdAndCode(c.GetInt64("req_id"), common.ErrLoginTimeout).Panicf("parse token err: %v", err)
			c.Abort()
			return
		} else {
			logger.WithReqIdAndCode(c.GetInt64("req_id"), common.ErrIllegalToken).Panicf("parse token err: %v", err)
			c.Abort()
			return
		}

		// 获取token中携带信息
		claims := &common.MyCustomClaims{}
		if reqClaims, ok := token.Claims.(*common.MyCustomClaims); ok {
			// 更新过期时间
			expireAt := time.Now().Add(time.Second * time.Duration(viper.GetInt("auth.token_expire_time")*86400))
			reqClaims.ExpiresAt = jwt.NewNumericDate(expireAt)

			claims = reqClaims
			logger.WithReqId(c.GetInt64("req_id")).Debugf("%v %v", claims.UserId, claims.RegisteredClaims.ExpiresAt)
		} else {
			panic(errno.NewCodeErr(common.ErrRequestTokenNil))
			c.Abort()
			return
		}

		c.Set("user_id", claims.UserId)
		c.Next()
	}
}
