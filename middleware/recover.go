package middleware

import (
	"runtime/debug"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"wh_backend/common"
	"helper/common/errno"
	"helper/common/logger"
)

func RecoverError() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			p := recover()
			c.Abort()

			// 自定义异常
			if codeErr, ok := p.(errno.CodeErr); ok {
				common.WriteResponse(c, codeErr, nil)
				return
			}

			// log日志异常
			if logPanic, ok := p.(*logrus.Entry); ok {
				msg := ""
				if logPanic.Data["show"] != nil { // 判断是否接口直接返回错误详情
					msg = logPanic.Message
				}
				codeErr := errno.NewCodeErrAndMsg(logPanic.Data["code"].(int), msg)
				common.WriteResponse(c, codeErr, nil)
				debug.PrintStack()
				return
			}

			// 其它异常
			if p != nil {
				logger.WithReqId(c.GetInt64("req_id")).Errorf("panic recover! p: %v", p)
				debug.PrintStack()
			}

			if err, ok := p.(error); ok {
				common.WriteResponse(c, err, nil)
				return
			}
		}()
		c.Next()
	}
}
