package middleware

import (
	"time"

	"github.com/gin-gonic/gin"

	"helper/common/logger"
	"helper/common/tools"
)

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 唯一请求id
		reqId := tools.UIdWorker().GetId()
		c.Set("req_id", reqId)

		// 开始时间
		start := time.Now()
		// 处理请求
		c.Next()
		// 结束时间
		end := time.Now()
		//执行时间
		latency := end.Sub(start)
		path := c.Request.URL.Path
		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()
		logger.WithReqId(reqId).Infof("| %3d | %13v | %15s | %s  %s |", statusCode, latency, clientIP, method, path)
	}
}
