// @title       wh_backend
// @version     1.0
// @description 计算器后端项目 golang.

// @license.name 项目地址
// @license.url  https://gitlab.com/npaaui/wh-backend

// @host     localhost:8090
// @BasePath /api/v1
package main

import (
	"os"
	"runtime"

	"github.com/gin-gonic/gin"
	"github.com/gookit/validate/locales/zhcn"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"helper/common"
	"helper/common/logger"
	"helper/common/tools"
	"wh_backend/app"
	_ "wh_backend/docs"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 唯一id生成器
	tools.InitWorker(viper.GetInt64("service.index"))

	// 初始化 配置
	dir, _ := os.Getwd()
	common.InitConfig(dir+string(os.PathSeparator)+"config", "ini")

	// 初始化 log日志
	logger.InitLogger(viper.GetString("logger.level"), viper.GetString("logger.folder"))

	// 初始化 路由
	r := gin.Default()
	app.AddRouter(r)

	// 初始化 swagger文档
	r.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// 加载语言包
	zhcn.RegisterGlobal()

	// 启动
	err := r.Run(viper.GetString("service.host") + ":" + viper.GetString("service.port"))
	logger.Instance.Errorf("error run: %v", err)
}
